import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {

    private  JTabbedPane Tabs;
    private JButton MargheritaButton;
    private JButton DeluxeMixButton;
    private JButton MayoMasterButton;
    private JButton TeraMeatButton;
    private JButton SeafoodMixButton;
    private JButton CheeseBombButton;
    private JButton CheckOutButton;
    private JButton YenDollarButton;
    private JButton CocaColaButton;
    private JButton GINGERALEButton;
    private JButton CafeLatteButton;
    private JButton CocaColaZeroButton;
    private JButton DrPepperButton;
    private JButton OrangeJuiceButton;
    private JButton OvenBakedPotatoWedgesButton;
    private JButton ChickenNuggetsButton;
    private JButton PopcornShrimpButton;
    private JPanel Panel;
    private JTextArea OrderedArea;
    private JTextArea MargheritaArea;
    private JButton CancellingTheOrderButton;
    private JLabel OrderdItems;
    private JLabel Total;
    private JLabel Unit;
    private JLabel MargheritaLabel;
    private JLabel DeluxeMixLabel;
    private JLabel MayoMasterLabel;
    private JLabel TeraMeatLabel;
    private JLabel SeafoodMixLabel;
    private JLabel CheeseBombLabel;
    private JLabel CocaColaLabel;
    private JLabel CocaColaZeroLabel;
    private JLabel DrPepperLabel;
    private JLabel GINGERALELabel;
    private JLabel OrangeJuiceLabel;
    private JLabel CafeLatteLabel;
    private JLabel OvenBakedPotatoWedgesLabel;
    private JLabel ChickenNuggetsLabel;
    private JLabel PopcornShrimpLabel;
    private JLabel UnitLabel1;
    private JLabel UnitLabel2;
    private JLabel UnitLabel3;
    private JLabel UnitLabel4;
    private JLabel UnitLabel5;
    private JLabel UnitLabel6;
    private JLabel UnitLabel7;
    private JLabel UnitLabel8;
    private JLabel UnitLabel9;
    private JLabel UnitLabel10;
    private JLabel UnitLabel11;
    private JLabel UnitLabel12;
    private JLabel UnitLabel13;
    private JLabel UnitLabel14;
    private JLabel UnitLabel15;
    float TruePrice;
    public FoodOrderingMachine() {

        MargheritaButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_1726_ja_hero_8246.jpg")
        ));
        MargheritaButton.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Margherita", Integer.parseInt(MargheritaLabel.getText()), Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Margherita", Float.parseFloat(MargheritaLabel.getText()), Unit.getText());
                }
            }
        });

        DeluxeMixButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_1602_ja_hero_7498.jpg")
        ));
        DeluxeMixButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Deluxe Mix",Integer.parseInt(DeluxeMixLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Deluxe Mix", Float.parseFloat(DeluxeMixLabel.getText()), Unit.getText());
                }


            }
        });

        MayoMasterButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_1609_ja_hero_7541.jpg")
        ));
        MayoMasterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Mayo Master",Integer.parseInt(MayoMasterLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Mayo Master", Float.parseFloat(MayoMasterLabel.getText()), Unit.getText());
                }

            }
        });

        TeraMeatButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_1610_ja_hero_3986.jpg")
        ));
        TeraMeatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Tera Meat",Integer.parseInt(TeraMeatLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Tera Meat", Float.parseFloat(TeraMeatLabel.getText()), Unit.getText());
                }

            }
        });

        SeafoodMixButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_1091_ja_hero_10195.jpg")
        ));
        SeafoodMixButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Seafood Mix",Integer.parseInt(SeafoodMixLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Seafood Mix", Float.parseFloat(SeafoodMixLabel.getText()), Unit.getText());

                }

            }
        });

        CheeseBombButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_2501_ja_hero_10151.jpg")
        ));
        CheeseBombButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Cheese Bomb",Integer.parseInt(CheeseBombLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Cheese Bomb", Float.parseFloat(CheeseBombLabel.getText()), Unit.getText());
                }

            }
        });

        CocaColaButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5338_ja_hero_4080.jpg")
        ));
        CocaColaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Coca Cola",Integer.parseInt(CocaColaLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Coca Cola", Float.parseFloat(CocaColaLabel.getText()), Unit.getText());

                }

            }
        });

        CocaColaZeroButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5339_ja_hero_4080.jpg")
        ));
        CocaColaZeroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Coca Cola Zero",Integer.parseInt(CocaColaZeroLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Coca Cola Zero", Float.parseFloat(CocaColaZeroLabel.getText()), Unit.getText());
                }

            }
        });

        DrPepperButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5383_ja_hero_2629.jpg")
        ));
        DrPepperButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Dr Pepper",Integer.parseInt(DrPepperLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Dr Pepper", Float.parseFloat(DrPepperLabel.getText()), Unit.getText());
                }

            }
        });

        GINGERALEButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5362_ja_hero_3524.jpg")
        ));
        GINGERALEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("GINGER ALE",Integer.parseInt(GINGERALELabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("GINGER ALE", Float.parseFloat(GINGERALELabel.getText()), Unit.getText());
                }

            }
        });

        CafeLatteButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5563_ja_hero_2629.jpg")
        ));
        CafeLatteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Cafe Latte",Integer.parseInt(CafeLatteLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Cafe Latte", Float.parseFloat(CafeLatteLabel.getText()), Unit.getText());
                }

            }
        });

        OrangeJuiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5693_ja_hero_4327.jpg")
        ));
        OrangeJuiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Orange Juice",Integer.parseInt(OrangeJuiceLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Orange Juice", Float.parseFloat(OrangeJuiceLabel.getText()), Unit.getText());
                }

            }
        });

        OvenBakedPotatoWedgesButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5517_ja_menu_9220.jpg")
        ));
        OvenBakedPotatoWedgesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Oven Baked Potato Wedges",Integer.parseInt(OvenBakedPotatoWedgesLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Oven Baked Potato Wedges", Float.parseFloat(OvenBakedPotatoWedgesLabel.getText()), Unit.getText());
                }

            }
        });

        ChickenNuggetsButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5502_ja_menu_9220.jpg")
        ));
        ChickenNuggetsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Chicken Nuggets",Integer.parseInt(ChickenNuggetsLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Chicken Nuggets", Float.parseFloat(ChickenNuggetsLabel.getText()), Unit.getText());
                }

            }
        });

        PopcornShrimpButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/JP_5525_ja_menu_2072.jpg")
        ));
        PopcornShrimpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Unit.getText()=="yen") {
                    YenOrder("Popcorn Shrimp",Integer.parseInt(PopcornShrimpLabel.getText()),Unit.getText());
                }else if (Unit.getText()=="dollar") {
                    DollarOrder("Popcorn Shrimp", Float.parseFloat(PopcornShrimpLabel.getText()), Unit.getText());
                }

            }
        });
        CancellingTheOrderButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to cancel the order?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "List cleared.");
                    OrderedArea.setText("");
                    Total.setText("0");
                    TruePrice = 0;
                }
            }
        });
        CheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is "+Total.getText()+" "+Unit.getText()+".");
                    OrderedArea.setText("");
                    Total.setText("0");
                    TruePrice = 0;
                }
            }
        });
        YenDollarButton.addActionListener(new ActionListener() {


            @Override
            public void actionPerformed(ActionEvent e) {

                if(Unit.getText()=="yen"){

                    Unit.setText("dollar");
                    UnitLabel1.setText("dollar");
                    UnitLabel2.setText("dollar");
                    UnitLabel3.setText("dollar");
                    UnitLabel4.setText("dollar");
                    UnitLabel5.setText("dollar");
                    UnitLabel6.setText("dollar");
                    UnitLabel7.setText("dollar");
                    UnitLabel8.setText("dollar");
                    UnitLabel9.setText("dollar");
                    UnitLabel10.setText("dollar");
                    UnitLabel11.setText("dollar");
                    UnitLabel12.setText("dollar");
                    UnitLabel13.setText("dollar");
                    UnitLabel14.setText("dollar");
                    UnitLabel15.setText("dollar");

                    TruePrice = Float.parseFloat(Total.getText())/130;
                    float DisplayPrice =((float)Math.round(TruePrice*1000))/1000;
                    Total.setText(String.valueOf(DisplayPrice));

                    MargheritaLabel.setText("14.615");
                    DeluxeMixLabel.setText("15");
                    MayoMasterLabel.setText("13.846");
                    TeraMeatLabel.setText("17.692");
                    SeafoodMixLabel.setText("18.615");
                    CheeseBombLabel.setText("14.385");
                    CocaColaLabel.setText("1.154");
                    CocaColaZeroLabel.setText("1.231");
                    DrPepperLabel.setText("1.192");
                    GINGERALELabel.setText("1.308");
                    CafeLatteLabel.setText("1.269");
                    OrangeJuiceLabel.setText("1.077");
                    OvenBakedPotatoWedgesLabel.setText("4.154");
                    ChickenNuggetsLabel.setText("4.615");
                    PopcornShrimpLabel.setText("5");

                    ListYenToDollar();

                } else if (Unit.getText()=="dollar") {
                    Unit.setText("yen");
                    UnitLabel1.setText("yen");
                    UnitLabel2.setText("yen");
                    UnitLabel3.setText("yen");
                    UnitLabel4.setText("yen");
                    UnitLabel5.setText("yen");
                    UnitLabel6.setText("yen");
                    UnitLabel7.setText("yen");
                    UnitLabel8.setText("yen");
                    UnitLabel9.setText("yen");
                    UnitLabel10.setText("yen");
                    UnitLabel11.setText("yen");
                    UnitLabel12.setText("yen");
                    UnitLabel13.setText("yen");
                    UnitLabel14.setText("yen");
                    UnitLabel15.setText("yen");
                    TruePrice = Float.parseFloat(Total.getText());

                    Total.setText(String.valueOf((Math.round(TruePrice*130))));

                    MargheritaLabel.setText("1900");
                    DeluxeMixLabel.setText("1950");
                    MayoMasterLabel.setText("1800");
                    TeraMeatLabel.setText("2300");
                    SeafoodMixLabel.setText("2420");
                    CheeseBombLabel.setText("1870");
                    CocaColaLabel.setText("150");
                    CocaColaZeroLabel.setText("160");
                    DrPepperLabel.setText("155");
                    GINGERALELabel.setText("170");
                    CafeLatteLabel.setText("165");
                    OrangeJuiceLabel.setText("140");
                    OvenBakedPotatoWedgesLabel.setText("540");
                    ChickenNuggetsLabel.setText("600");
                    PopcornShrimpLabel.setText("650");

                    ListDollarToYen();
                }

            }
        });
    }

    void YenOrder(String foodName,int price,String unit){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+foodName+"?\nIt will be "+ price +" "+ unit,
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,"Order for " + foodName + " received.");
            String currentText = OrderedArea.getText();
            OrderedArea.setText(currentText +foodName+"   "+price+" "+unit+ "\n");

            int currentPrice = Integer.parseInt(Total.getText());
            String nowPrice = String.valueOf(currentPrice + price);
            Total.setText(nowPrice);
        }
    }

    void DollarOrder(String foodName,float price,String unit){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+foodName+"?\nIt will be "+ price +" "+ unit,
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,"Order for " + foodName + " received.");
            String currentText = OrderedArea.getText();
            OrderedArea.setText(currentText +foodName+"   "+price+" "+unit+ "\n");

            float currentPrice = Float.parseFloat(Total.getText());
            float DisplayPrice =((float)Math.round((currentPrice + price)*1000))/1000;
            String nowPrice = String.valueOf(DisplayPrice);
            Total.setText(nowPrice);
        }
    }
    void ListYenToDollar(){
        OrderedArea.setText(OrderedArea.getText().replace("yen","dollar"));
        OrderedArea.setText(OrderedArea.getText().replace(" 1900 "," 14.615 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1950 "," 15 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1800 "," 13.846 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 2300 "," 17.692 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 2420 "," 18.615 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1870 "," 14.385 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 150 "," 1.154 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 160 "," 1.231 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 155 "," 1.192 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 170 "," 1.308 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 165 "," 1.269 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 140 "," 1.077 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 540 "," 4.154 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 600 "," 4.615 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 650 "," 5 "));
    }

    void ListDollarToYen(){
        OrderedArea.setText(OrderedArea.getText().replace("dollar","yen"));
        OrderedArea.setText(OrderedArea.getText().replace(" 14.615 "," 1900 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 15 "," 1950 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 13.846 "," 1800 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 17.692 "," 2300 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 18.615 "," 2420 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 14.385 "," 1870 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1.154 "," 150 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1.231 "," 160 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1.192 "," 155 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1.308 "," 170 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1.269 "," 165 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 1.077 "," 140 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 4.154 "," 540 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 4.615 "," 600 "));
        OrderedArea.setText(OrderedArea.getText().replace(" 5 "," 650 "));
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().Panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
